module.exports = {
  chainWebpack: (config) => {
    const svgRule = config.module.rule("svg");

    svgRule.uses.clear();

    if (process.env.use_analyzer) {
      config
        .plugin("webpack-bundle-analyzer")
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        .use(require("webpack-bundle-analyzer").BundleAnalyzerPlugin);
    }

    config.module
      .rule("svg")
      .test(/\.svg$/)
      .use("html-loader")
      .loader("html-loader");
  },
};
