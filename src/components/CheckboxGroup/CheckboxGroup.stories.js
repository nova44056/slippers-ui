import SlpCheckboxGroup from "./CheckboxGroup";
import SlpCheckboxGroupTest from "./CheckboxGroupTest.vue";

export default {
  title: "Components/CheckboxGroup",
  component: SlpCheckboxGroup,
  argTypes: {
    orientation: {
      control: { type: "select" },
      options: ["vertical", "horizontal"],
    },
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { SlpCheckboxGroup },
  props: Object.keys(argTypes),
  template: '<SlpCheckboxGroup v-bind="$props">RadioGroup</SlpCheckboxGroup>',
});

const CheckboxGroupTest = (args, { argTypes }) => ({
  components: { SlpCheckboxGroupTest },
  props: Object.keys(argTypes),
  template: `<SlpCheckboxGroupTest v-bind="$props"></SlpCheckboxGroupTest>`,
});

export const Default = Template.bind({});
Default.args = {
  options: ["first", "second", "third", "fourth"],
  name: "example",
};

export const Test = CheckboxGroupTest.bind({});
