import SlpCheckbox from "./Checkbox";
import SlpCheckboxTest from "./CheckboxTest.vue";

export default {
  title: "Components/Checkbox",
  component: SlpCheckbox,
  argTypes: {},
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { SlpCheckbox },
  props: Object.keys(argTypes),
  template: '<SlpCheckbox v-bind="$props">Radio</SlpCheckbox>',
});

const CheckboxTest = (args, { argTypes }) => ({
  components: { SlpCheckboxTest },
  props: Object.keys(argTypes),
  template: `<SlpCheckboxTest v-bind="$props"></SlpCheckboxTest>`,
});

export const Default = Template.bind({});
Default.args = {
  option: "Option",
  name: "example",
};

export const Test = CheckboxTest.bind({});
