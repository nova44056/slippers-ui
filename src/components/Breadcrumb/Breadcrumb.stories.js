import SlpBreadcrumb from "../Breadcrumb/Breadcrumb.vue";

export default {
  title: "Components/Breadcrumb",
  component: SlpBreadcrumb,
  argTypes: {},
};

const Template = (args, { argTypes }) => {
  return {
    components: { SlpBreadcrumb },
    props: Object.keys(argTypes),
    template: `<div :style="{paddingTop: '150px'}"><SlpBreadcrumb v-bind="$props"/></div>`,
  };
};

export const Default = Template.bind({});
Default.args = {
  crumbs: [
    {
      title: "Get Started",
      path: "/",
    },
    {
      title:
        "Get Started for Enterprise and for Students and very long test for mobile",
      path: "/",
    },
    {
      title: "Get Started for Small Business",
    },
  ],
};
