import SlpContainer from "../Container/Container.vue";
import SlpRow from "../Row/Row.vue";
import SlpColumn from "../Column/Column.vue";

export default {
  title: "Layout/Column",
  component: SlpColumn,
  argTypes: {
    size: {
      control: { type: "select" },
      options: ["xs", "sm", "md", "lg", "xl", "xxl"],
    },
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { SlpContainer, SlpRow, SlpColumn },
  props: Object.keys(argTypes),
  template: `<SlpContainer><SlpRow><SlpColumn v-bind="$props">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer auctor ipsum vel euismod commodo. Ut ullamcorper pellentesque lacus quis malesuada. Nulla quis posuere tortor. Curabitur vel commodo orci. In diam orci, interdum et enim sit amet, finibus sodales dolor. Quisque luctus neque sit amet nunc pellentesque commodo sed et tortor. Nunc porttitor rutrum dui, eget lacinia nulla.
    Donec volutpat in purus sed sagittis. Praesent lobortis velit id quam auctor, in dictum nulla ornare. Phasellus non metus ligula. Integer venenatis tellus nec tellus cursus iaculis. Phasellus ut pellentesque metus, non maximus libero. Sed mi libero, ultricies eget bibendum vitae, eleifend eget massa. Aliquam tempor turpis id malesuada facilisis. Aenean rhoncus massa lacus, non aliquam nunc vehicula quis. Proin massa odio, vestibulum eu eros in, fermentum pulvinar metus. Vivamus vel vulputate orci, vel molestie ligula. In vel turpis ac lacus efficitur semper. Donec sit amet sapien in est dapibus finibus non sit amet turpis. Phasellus nec scelerisque sapien. Etiam risus quam, ultricies vel leo quis, commodo commodo tortor.
    Nulla facilisi. Nulla congue tellus non dolor dapibus, in tempor sem lobortis. Morbi eleifend nisi magna, eget malesuada nisi molestie id. Donec malesuada libero in tellus rhoncus, eget blandit urna ornare. Fusce vulputate eleifend mi porta fermentum. In faucibus, quam ac consequat lacinia, odio velit convallis ante, sed ullamcorper urna elit non nisi. Ut tincidunt non est eget sodales. Pellentesque ac lectus at massa consectetur venenatis. In eget tellus eget velit dapibus aliquam. Phasellus nec tellus non urna varius lacinia. Praesent fringilla, nunc nec luctus consectetur, nibh nibh vehicula sapien, in egestas nibh risus ut libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</SlpColumn></SlpRow></SlpContainer>`,
});

export const Default = Template.bind({});
Default.args = {
  cols: 12,
  size: "md",
};
