import SlpRadioGroup from "./RadioGroup";
import SlpRadioGroupTest from "./RadioGroupTest.vue";

const defaultOptions = [
  { value: "first", name: "name" },
  { value: "second", name: "name" },
  { value: "third", name: "name" },
  { value: "fourth", disabled: true, name: "name" },
];

export default {
  title: "Components/RadioGroup",
  component: { SlpRadioGroup },
  argTypes: {
    orientation: {
      control: { type: "select" },
      options: ["vertical", "horizontal"],
    },
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { SlpRadioGroup },
  props: Object.keys(argTypes),
  template: `
  <SlpRadioGroup
  v-bind="$props"
  :options="options"
  :name="name"
  v-model="selected"
  >
  </SlpRadioGroup>`,
  data() {
    return {
      selected: "first",
    };
  },
});

const RadioGroupTest = (args, { argTypes }) => ({
  components: { SlpRadioGroupTest },
  props: Object.keys(argTypes),
  template: `<SlpRadioGroupTest v-bind="$props"></SlpRadioGroupTest>`,
});

export const Default = Template.bind({});
Default.args = {
  options: defaultOptions,
};

Default;
export const Test = RadioGroupTest.bind({});
