import Container from "./Container/Container";
import Column from "./Column/Column";
import SlpTypography from "./Typography/Typography";
import data from "../content/overview.json";

export default {
  title: "Components",
};

const Template = (args, { argTypes }) => ({
  components: { Container, Column, SlpTypography },
  props: Object.keys(argTypes),
  methods: {},
  data() {
    return {
      data: data,
      env: process.env.NODE_ENV,
    };
  },
  template: `
    <section class='slp-storybook-components-overview slp-mt-64'>
    <Container>
      <header class='slp-mx-16 slp-mb-32'>
        <h1 class='slp-mb-32'>Slippers components</h1>
        <SlpTypography tag='p' variant='body1'>Slippers components are written in Vue and designed in Figma. Select a component to learn more about how to use them within our marketing site.</SlpTypography>
      </header>
      <div class='slp-storybook-components-overview__body'>
      <Column class='slp-storybook-components-overview__component-column' :cols='4' v-for='component in data'>
        <a class='slp-storybook-components-overview__component-card' :href="env === 'development'?component.url:'/marketing/digital-experience/slippers-ui'+component.url">
          <div>
            <SlpTypography tag='h2' variant='body1-bold'>{{component.name}}</SlpTypography>
            <SlpTypography tag='p' variant='body2'>{{component.description}}</SlpTypography>
          </div>
        </a>
      </Column>
    </div>
    </Container>
  </section>
  `,
});

export const Overview = Template.bind({});
