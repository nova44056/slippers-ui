import Icon from "./Icon";

const requireIcons = require.context("../../static/icons/", false, /.svg$/);

const availableIcons = requireIcons
  .keys()
  .map((fileName) => `slp-${fileName.replace(/^\.\/(.+)\.svg$/, "$1")}`);

export default {
  title: "Components/Icon",
  component: Icon,
  argTypes: {
    icon: {
      control: { type: "select" },
      options: [
        "gl-archive",
        "gl-attention-solid",
        "gl-clear",
        "gl-heart",
        "gl-incognito",
        ...availableIcons,
      ],
    },
    size: {
      control: { type: "select" },
      options: ["xs", "sm", "md", `lg`, "xl"],
    },
    color: {
      control: { type: "select" },
      options: [
        "black",
        "surface-300",
        "surface-950",
        "#000000",
        "#aaf",
        "#f04",
      ],
    },
  },
};

const TemplateNew = (args, { argTypes }) => ({
  components: { Icon },
  props: Object.keys(argTypes),
  template: `
    <section class='slp-storybook-icon-gallery slp-mt-64'>
      <Container>
        <div class='slp-mx-16 slp-mb-32'>
          <h1 class='slp-mb-32'>How to use</h1>
          <p>Fill the <b>icon</b> property with one icon name from our catalog, just add the <strong>slp-</strong> prefix.</p>
          <p class='slp-mb-16'>If you want to use some of the <a
          href='https://gitlab-org.gitlab.io/gitlab-svgs/' target='_blank'>GitLab SVGs</a>, use the <strong>gl-</strong> prefix instead 
          </p>
          <p class='slp-mb-16'>You can use the <b>color</b> property with Hex color code or <pre>Slp</pre> classes </p>
        </div>
        <div class='slp-storybook-icon-gallery__body'>
          <Column :cols='4'>
            <div class='slp-storybook-icon-gallery__icon-card'>
              <h5 class='slp-mb-8'>{{ icon }}</h5>
              <div class='slp-storybook-icon-gallery__icons'>
                <div class='slp-mx-8'>
                  <Icon style="padding:30px" v-bind="$props" >{{icon}}</Icon>
                  <p>{{ size }}</p>
                </div>
              </div>
            </div>
          </Column>
        </div>
      </Container>
    </section>
    `,
});

export const Default = TemplateNew.bind({});
Default.args = {
  icon: "slp-crown",
  size: "md",
  color: "surface-300",
};

const Template = (args, { argTypes }) => ({
  components: { Icon },
  props: Object.keys(argTypes),
  template: `
    <div style="padding: 50px; display: flex; flex-direction: column">
      <p>The <b>variant</b>,  <b>name</b>, <b>hexColor</b> and  <b>slpColor</b> are deprecated, please use the unified style.</p>
      <Icon name="archive" variant="product" size="lg" hexColor="#4f0"/>
      <Icon name="article" variant="marketing" size="lg" hexColor="#f00"/>
      <Icon name="archive" variant="product" size="lg" slpColor="success-100"/>
      <Icon name="bullet" variant="marketing" size="lg" slpColor="accent-500"/>

      <p>You can click <b>Show code</b> below to see the previous usage </p>
    </div>`,
});

export const Deprecated = Template.bind({});
Deprecated.args = {
  name: "archive",
  variant: "product",
  slpColor: "surface-300",
};
