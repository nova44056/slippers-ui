import SlpSideNavigation from "./SideNavigation.vue";
import SlpContainer from "../../components/Container/Container.vue";

export default {
  title: "Compositions/SideNavigation",
  component: SlpSideNavigation,
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => {
  return {
    components: { SlpContainer, SlpSideNavigation },
    props: Object.keys(argTypes),
    template: `
      <div>
        <div class="placeholder-nav">Placeholder nav</div>
        <div class="intersection-observer">Intersection observer</div>
        <SlpContainer>
          <SlpSideNavigation v-bind="$props">
            <template v-slot:header>
              <h2>Custom header slot</h2>
            </template>
            <template v-slot:footer>
              <h2>Custom footer slot</h2>
            </template>
            <div class="side-navigation-story">
              <h1 id="contact-support">Contact us</h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean 
                commodo pulvinar massa id sodales. Nullam nunc lorem, convallis 
                et ultrices sit amet, sagittis et leo. Fusce vestibulum odio ut 
                sodales feugiat. Curabitur at mi feugiat tellus consequat efficitur 
                at quis massa. Pellentesque a augue sed ante dictum posuere in 
                vitae lectus. Morbi luctus tincidunt nisl, ut aliquam enim 
                consequat faucibus. Nunc nec nulla lorem.
              </p>
              <h2 id="issues">Issues</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean 
              commodo pulvinar massa id sodales. Nullam nunc lorem, convallis 
              et ultrices sit amet, sagittis et leo.</p>

              <h1 id="gitlab-support-service-levels">GitLab Support Services Levels</h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean 
                commodo pulvinar massa id sodales. Nullam nunc lorem, convallis 
                et ultrices sit amet, sagittis et leo. Fusce vestibulum odio ut 
                sodales feugiat. Curabitur at mi feugiat tellus consequat efficitur 
                at quis massa. Pellentesque a augue sed ante dictum posuere in 
                vitae lectus. Morbi luctus tincidunt nisl, ut aliquam enim 
                consequat faucibus. Nunc nec nulla lorem.
              </p>
              <h2 id="trials-support">Trials Support</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean 
              commodo pulvinar massa id sodales. Nullam nunc lorem, convallis 
              et ultrices sit amet, sagittis et leo.</p>
              <h2 id="standard-support-legacy">Standard Support</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean 
              commodo pulvinar massa id sodales. Nullam nunc lorem, convallis 
              et ultrices sit amet, sagittis et leo.</p>
              <h2 id="priority-support">Priority Support</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean 
              commodo pulvinar massa id sodales. Nullam nunc lorem, convallis 
              et ultrices sit amet, sagittis et leo.</p>

              <h1 id="further-resources">Further Resources</h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean 
                commodo pulvinar massa id sodales. Nullam nunc lorem, convallis 
                et ultrices sit amet, sagittis et leo. Fusce vestibulum odio ut 
                sodales feugiat. Curabitur at mi feugiat tellus consequat efficitur 
                at quis massa. Pellentesque a augue sed ante dictum posuere in 
                vitae lectus. Morbi luctus tincidunt nisl, ut aliquam enim 
                consequat faucibus. Nunc nec nulla lorem.
              </p>
            </div>
          </SlpSideNavigation>
        </SlpContainer>
      </div>
    `,
  };
};

export const Default = Template.bind({});
Default.args = {
  data: {
    anchors: {
      text: "ON THIS PAGE",
      data: [
        {
          text: "Contact Support",
          href: "#contact-support",
          data_ga_name: "test_ga_name",
          data_ga_location: "test_ga_location",
          nodes: [
            {
              text: "Issues",
              href: "#issues",
            },
          ],
        },
        {
          text: "GitLab Support Service Levels",
          href: "#gitlab-support-service-levels",
          nodes: [
            {
              text: "Trials Support",
              href: "#trials-support",
              data_ga_name: "test_ga_name",
              data_ga_location: "test_ga_location",
            },
            {
              text: "Standard Support",
              href: "#standard-support-legacy",
            },
            {
              text: "Priority Support",
              href: "#priority-support",
            },
          ],
        },
        {
          text: "Further Resources",
          href: "#further-resources",
        },
      ],
    },
    hyperlinks: {
      text: "MORE ON THIS TOPIC",
      data: [
        {
          text: "Google",
          href: "http://www.google.com",
        },
        {
          text: "Amazon",
          href: "http://www.amazon.com",
        },
      ],
    },
  },
};
