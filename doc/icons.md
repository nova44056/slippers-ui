# Slippers Icons Component

All the icons used in the Digital Experience projects are located in Slippers
to avoid duplicating images across all projects and to have a single source of truth in terms of icons.

## How to use Icons Component

The component is called `SlpIcon` and it has the props marked below:

| Prop | Description | Required | Default Value |
|------|-------------|----------|---------------|
| name | The name of the icon. It can be an Icon from [GitLab SVGS](https://gitlab-org.gitlab.io/gitlab-svgs/) or a custom Icon from Slippers located in `src/static/icons` | Yes | N/A |
| variant | It can be `marketing` for slippers icons or `product` for `@gitlab/svgs` icons | Yes | N/A | 
| size | The size of the icon | No | `md` |
| hexColor | Any color in HEX format (i.e `#555`). Should not be used together with `slpColor` property | No | N/A |
| slpColor | Any color from Slippers color list (i.e `primary-200`). Should not be used together with `hexColor` property | No | N/A |

## How to add a new Icon to the library

In order to correctly add a new icon to the icon library the following steps must be completed:

1. Verify that the icon does not exist and put the new icon in the icons folder `src/static/icons`.
   1. The icon must be in `svg` format. 
   2. If the new icon is a variant from an existent one, use the naming convention `icon_name-alt`.
      1. It is necessary to verify that the name of the icon does not exist in [GitLab SVGS](https://gitlab-org.gitlab.io/gitlab-svgs/), if it exists, the naming convention mentioned above must be used.
2. Ensure that all color properties can be applied to the icon using Storybook.
   1. If the color properties does not work correctly for mono-color icons, the `svg` file must be modified.
      1. Commonly, the `fill` and `stroke` properties should have `currentColor` as value.

## Optimize icons

In order to have a smaller bundle size, when a new icon is added, the following script should be run
to optimize all the `svg` files inside `src/static/icons`

- `npm run optimize-svg-icons`