# Slippers UI - GitLab's Marketing Design System

Slippers is the open-source GitLab Marketing Web Design System. It was created in the spirit of ["everyone can contribute"](https://about.gitlab.com/company/mission/#everyone-can-contribute).

## Resources:
- Storybook: https://gitlab-com.gitlab.io/marketing/digital-experience/slippers-ui/.
- Figma: https://www.figma.com/file/nWIOpmuMp7RZXmfTj6ujAF/Slippers_foundations.
- GitLab project: https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui.
- Further documentation: https://about.gitlab.com/handbook/marketing/digital-experience/slippers-design-system/.


## Gettings started with Slippers:
### Local development
Slippers uses [Storybook](https://storybook.js.org/) to enable component exploration.

[Yarn](https://yarnpkg.com/) is the preferred package manager.

**Running Storybook locally:**
1. Clone the [Slippers project](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui).
2. Install the dependecies using the version of node specified in [.npmrc](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui/-/blob/main/.npmrc) - `npm install`.
3. Run Storybook - `yarn storybook`.
4. Storybook should now be running at: http://localhost:6009/.


**Updating the slippers-ui NPM package**
1. Make changes to slippers-ui.
2. If needed, create/update .stories so Storybook is upto date.
3. Increment the version number in package.json.
4. If needed, add component into install.js (this adds components into the build).
5. Build /dist folder - `yarn build library`.
6. Publish to NPM - `yarn publish`.
7. Merge changes into origin/main.
8. Once the package has been published it's now ready to be updated in the consuming repositories (Example: Buyer Experience, be-navigation, etc.).


**Importing slippers-ui into your project:**
- With NPM `npm install slippers-ui` OR with YARN `yarn add slippers-ui` 
- Slippers main entry point is /dist/slippers-core.umd.min.js, [example usage](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/blob/main/plugins/slippers-ui.ts). To import the styling for the above components, the compiled css file can be found at: /dist/slippers-core.css.
