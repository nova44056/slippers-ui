import slippersTheme from "./SlippersTheme";

import "../src/styles/base.scss";
import "../src/styles/storybook.scss";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  docs: {
    theme: slippersTheme,
  },
  options: {
    storySort: {
      order: [
        "Intro",
        "Foundations",
        ["Icons", "Color", "Space"],
        "Components",
        ["Overview", "*"],
      ],
    },
  },
};
